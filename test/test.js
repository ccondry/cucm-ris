const cucmRis = require('../index')
const util = require('util')
// create axl connection object
const ris = new cucmRis({
  host: 'cucm1.dcloud.cisco.com',
  user: 'axluser',
  pass: 'C1sco12345',
  version: '11.5'
})

describe('getStatus()', function () {
  it('should show status of devices on CUCM using wildcard on directory number', function (done) {
    ris.getStatus('*377')
    .then(results => {
      console.log('results.length', results.length)
      done()
    }).catch(e => done(e))
  })
})

describe('getStatus()', function () {
  it('should show status of devices on CUCM using wildcard on directory number', function (done) {
    ris.getStatus('*377', 'DirNumber')
    .then(results => {
      console.log('results.length', results.length)
      done()
    }).catch(e => done(e))
  })
})

describe('getStatus()', function () {
  it('should show status of devices on CUCM using list of devices by name', function (done) {
    ris.getStatus(['CTIRD1300', 'CTIRD1301'], 'Name')
    .then(results => {
      console.log('results.length', results.length)
      done()
    }).catch(e => done(e))
  })
})
