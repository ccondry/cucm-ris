const axios = require('axios')
const parseXmlString = require('./parse-xml')
const js2xmlparser = require('js2xmlparser')
const getArray = require('./get-array')

// capitalize the first letter of a string and return it
function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

class Ris {
  constructor(settings) {
    this.host = settings.host
    this.user = settings.user
    this.pass = settings.pass
    this.version = settings.version

    this.js2xmlOptions = {
      attributeString: '$',
      declaration: {
        include: false
      }
    }
  }

  /*** main AXL command runner ***/
  async run ({
    maxReturnedDevices = 20,
    deviceClass = 'Any',
    status = 'Any',
    selectBy = 'Name',
    innerBody
  }) {
    const url = `https://${this.host}:8443/realtimeservice/services/RisPort`
    const basicAuth = new Buffer(this.user + ':' + this.pass).toString('base64')
    const headers = {
      'Authorization': `Basic ${basicAuth}`,
      'Content-Type': 'text/xml',
      'SOAPAction': `"http://schemas.cisco.com/ast/soap/action/#RisPort#SelectCmDevice"`
    }
    const body = `<?xml version="1.0" encoding="utf-8" standalone="no"?>
    <soapenv:Envelope
    xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
    xmlns:tns="http://schemas.cisco.com/ast/soap/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <soapenv:Body>
    <SelectCmDevice>
    <StateInfo xsi:type="xsd:string"/>
    <CmSelectionCriteria>
    <MaxReturnedDevices xsi:type="xsd:unsignedInt">${maxReturnedDevices}</MaxReturnedDevices>
    <Class xsi:type="xsd:string">${deviceClass}</Class>
    <Status xsi:type="xsd:string">${status}</Status>
    <NodeName xsi:nil="true" xsi:type="xsd:string"/>
    <SelectBy xsi:type="xsd:string">${selectBy}</SelectBy>
    <SelectItems xsi:type="soapenc:Array">
    ${innerBody}
    </SelectItems>
    </CmSelectionCriteria>
    </SelectCmDevice>
    </soapenv:Body>
    </soapenv:Envelope>`

    try {
      console.log(`running cucm-ris request to SelectCmDevice`)
      // POST request to CUCM
      const res = await axios.post(url, body, {headers})
      // parse XML to JSON
      const json = await parseXmlString(res.data)
      // extract and return relevant response data
      const soapBody = json['soapenv:Envelope']['soapenv:Body']
      // return soapBody.SelectCmDeviceResponse.SelectCmDeviceResult.CmNodes.item
      const ret = []
      // console.log(soapBody.SelectCmDeviceResponse.SelectCmDeviceResult.CmNodes)
      let itemList = []
      try {
        itemList = getArray(soapBody.SelectCmDeviceResponse.SelectCmDeviceResult.CmNodes.item[0].CmDevices.item)
      } catch (e) {
      }
      for (let item of itemList) {
        ret.push({
          name: item.Name._,
          ipAddress: item.IpAddress._,
          dirNumber: item.DirNumber._,
          deviceClass: item.Class._,
          model: item.Model._,
          product: item.Product._,
          httpd: item.Httpd._,
          registrationAttempts: item.RegistrationAttempts._,
          isCtiControllable: item.IsCtiControllable._,
          loginUserId: item.LoginUserId._,
          status: item.Status._,
          statusReason: item.StatusReason._,
          perfMonObject: item.PerfMonObject._,
          dChannel: item.DChannel._,
          h323Trunk: item.H323Trunk,
          description: item.Description._,
          timestamp: item.TimeStamp._,
        })
      }
      return ret
    } catch (e) {
      let errorMessage
      try {
        // try to parse the xml error
        if (e.response && e.response.data) {
          const json = await parseXmlString(e.response.data)
          errorMessage = json['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']['faultstring']
        } else {
          errorMessage = e
        }
      } catch (e2) {
        // console.log(e2)
        errorMessage = e2
        // if parsing fails, just throw the original error
        // throw e
      }
      throw errorMessage
    }
  }

  getStatus (match, selectBy = 'DirNumber') {
    let innerBody = ''
    if (Array.isArray(match)) {
      // multiple entries
      for (let item of match) {
        innerBody += `<item>
        <Item>${item}</Item>
        </item>`
      }
    } else {
      // single entry
      innerBody += `<item>
      <Item>${match}</Item>
      </item>`
    }
    // console.log(innerBody)
    // console.log('selectBy', selectBy)
    return this.run({selectBy, innerBody})
  }

}

module.exports = Ris
