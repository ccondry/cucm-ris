module.exports = function (data) {
  if (Array.isArray(data)) {
    return data
  } else if (typeof data === 'object') {
    return [data]
  } else {
    return []
  }
}
